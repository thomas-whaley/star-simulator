var AudioManager = (function () {
    function AudioManager() {
        this.freq = 50;
        this.res = 10;
        this.started = false;
        this.osc = new p5.Oscillator(100, 'triangle');
        this.verb = new p5.Reverb();
        this.filter = new p5.LowPass();
        this.filter.freq(this.freq);
        this.filter.res(this.res);
        this.osc.disconnect();
        this.osc.connect(this.filter);
        this.osc.amp(0.3);
        this.osc.freq(this.freq);
    }
    AudioManager.prototype.startAudio = function () {
        this.started = true;
        this.osc.start();
        this.verb.process(this.filter, 3, 2);
    };
    AudioManager.prototype.requestFreqChange = function (newFreq) {
        if (newFreq != this.freq) {
            this.freq = newFreq;
            this.filter.freq(newFreq, 5);
        }
    };
    AudioManager.prototype.requestResChange = function (newRes) {
        if (newRes != this.res) {
            this.res = newRes;
            this.osc.amp(newRes, 5);
        }
    };
    return AudioManager;
}());
var Constellation = (function () {
    function Constellation() {
        this.stars = [];
    }
    Constellation.prototype.update = function () {
    };
    Constellation.prototype.draw = function () {
    };
    return Constellation;
}());
var StarEdge = (function () {
    function StarEdge() {
        this.distance = 0;
        this.maxDistance = random(40, 60);
        this.distancePercent = 0;
        this.lifetime = 0;
        this.maxLifetime = random(60 * 5, 60 * 15);
        this.lifetimePercent = 0;
    }
    StarEdge.prototype.update = function () {
        this.distance = dist(this.from.x, this.from.y, this.to.x, this.to.y);
        this.distancePercent = this.distance / this.maxDistance;
        this.lifetime++;
        this.lifetimePercent = this.lifetime / this.maxLifetime;
    };
    StarEdge.prototype.f = function (x, a, b) {
        return exp(-((a * x - b) * (a * x - b)));
    };
    StarEdge.prototype.draw = function () {
        if (this.distancePercent > 1) {
            return;
        }
        var percent = 6.7 * this.distancePercent * (this.distancePercent - 1) * (this.distancePercent - 1);
        var o = 6.7 * this.lifetimePercent * (this.distancePercent - 1) * (this.distancePercent - 1);
        strokeWeight(0.5);
        stroke(lerpColor(color(255, 0), color(255, 180), percent * o));
        line(this.from.x, this.from.y, this.to.x, this.to.y);
    };
    return StarEdge;
}());
var NeighbourFinder = (function () {
    function NeighbourFinder(points, radius) {
        var _this = this;
        this.gridMap = new Map();
        this.points = points;
        this.radius = radius;
        this.points.forEach(function (p) { return _this.addStar(p); });
    }
    NeighbourFinder.prototype.addStar = function (star) {
        var key = this.makeKeyFromPosition(star.x, star.y);
        star.mysteriousKey = key;
        if (!this.gridMap.has(key)) {
            this.gridMap.set(key, new Set());
        }
        this.gridMap.get(key).add(star);
    };
    NeighbourFinder.prototype.updateStar = function (star) {
        if (!this.starDifferentKey(star)) {
            return;
        }
        this.removeStar(star);
        this.addStar(star);
    };
    NeighbourFinder.prototype.removeStar = function (star) {
        if (!this.gridMap.has(star.mysteriousKey)) {
            return;
        }
        var stars = this.gridMap.get(star.mysteriousKey);
        stars.delete(star);
        if (stars.size == 0) {
            this.gridMap.delete(star.mysteriousKey);
        }
    };
    NeighbourFinder.prototype.starDifferentKey = function (star) {
        return this.makeKeyFromPosition(star.x, star.y) != this.makeKeyFromPosition(star.prevX, star.prevY);
    };
    NeighbourFinder.prototype.makeKeyFromPosition = function (x, y) {
        var scaledX = int(x / this.radius);
        var scaledY = int(y / this.radius);
        return this.makeKey(scaledX, scaledY);
    };
    NeighbourFinder.prototype.makeKey = function (x, y) {
        return int(x) + "," + int(y);
    };
    NeighbourFinder.prototype.moveStar = function (star) {
        this.removeStar(star);
        this.addStar(star);
    };
    NeighbourFinder.prototype.query = function (star, minRadius) {
        var _this = this;
        if (minRadius === void 0) { minRadius = 0; }
        var x = int(star.x / this.radius);
        var y = int(star.y / this.radius);
        var neighbours = [];
        for (var i = -1; i <= 1; i++) {
            for (var j = -1; j <= 1; j++) {
                var keyX = x + i;
                var keyY = y + j;
                var key_1 = this.makeKey(keyX, keyY);
                if (!this.gridMap.has(key_1)) {
                    continue;
                }
                this.gridMap.get(key_1).forEach(function (s) {
                    if (neighbours.length > star.maxConnections) {
                        return;
                    }
                    var dst = distSq(s.x, s.y, star.x, star.y);
                    if (dst > _this.radius * _this.radius || dst < minRadius * minRadius) {
                        return;
                    }
                    neighbours.push(s);
                });
            }
        }
        return neighbours;
    };
    return NeighbourFinder;
}());
var DetectedObject = (function () {
    function DetectedObject() {
    }
    return DetectedObject;
}());
var ObjectDetector = (function () {
    function ObjectDetector() {
        this.detections = [];
        this.enabled = true;
        this.loaded = false;
    }
    ObjectDetector.prototype.init = function () {
        this.detector = ml5.objectDetector('cocossd');
        this.video = createCapture(VIDEO);
        this.video.hide();
    };
    ObjectDetector.prototype.detect = function (self) {
        self.detector.detect(self.video, function (e, r) { return self.gotDetections(self, e, r); });
    };
    ObjectDetector.prototype.gotDetections = function (self, error, results) {
        if (!this.enabled) {
            return;
        }
        if (error) {
            console.error(error);
            return;
        }
        self.detections = results.map(function (r) {
            var o = new DetectedObject();
            o.x = r.x;
            o.y = r.y;
            o.w = r.width;
            o.h = r.height;
            o.label = r.label;
            o.confidence = r.confidence;
            return o;
        }).filter(function (r) { return r.label == "person"; });
    };
    ObjectDetector.prototype.draw = function () {
        var _this = this;
        this.detections.forEach(function (o) {
            stroke(0, 255, 0);
            strokeWeight(4);
            noFill();
            var x = map(o.x, 0, _this.video.width, 0, width);
            var y = map(o.y, 0, _this.video.height, 0, height);
            var w = map(o.w, 0, _this.video.width, 0, width);
            var h = map(o.h, 0, _this.video.height, 0, height);
            rect(x, y, w, h);
            strokeWeight(1);
            stroke(255, 0, 0);
            line(x, y, x + w, y + h);
            line(x, y + h, x + w, y);
            noStroke();
            fill(255);
            textSize(24);
            text(o.label, x + 10, y + 24);
        });
    };
    ObjectDetector.prototype.getPoints = function () {
        var _this = this;
        return this.detections.map(function (o) { return createVector(map(o.x + o.w / 2, 0, _this.video.width, 0, width), map(o.y + o.h / 2, 0, _this.video.height, 0, height)); });
    };
    return ObjectDetector;
}());
p5.disableFriendlyErrors = true;
var stars = [];
var shootingStars = [];
var brightestStars = [];
var mousePointerGuide;
var osc;
var audioManager;
var constellationSoundFiles;
var shootingStarSoundFiles;
var NEIGHBOUR_SEARCH_RADIUS = 40;
var neighbourFinder;
var NUM_STARTING_STARS = 4000;
var objectDetector;
function distSq(x1, y1, x2, y2) {
    return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
}
function preload() {
    constellationSoundFiles = new SoundFileManager();
    constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Dark Bellphone_bip01.wav'));
    constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Distant Obscure Beacon_bip02.wav'));
    constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Dreams Illuminated_bip01.wav'));
    constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Glacier Reflections_bip_104.wav'));
    constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Glacier Reflections_bip_203.wav'));
    constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Hall Of the Stone Church_bip07.wav'));
    constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Living Room_bip05.wav'));
    constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Lush Multi Layer 1_bip03.wav'));
    constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Stargazer_bip08.wav'));
    constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Subsonic_bip_106.wav'));
    constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Vector Untitled_bip02.wav'));
    shootingStarSoundFiles = new SoundFileManager();
    shootingStarSoundFiles.addSoundFile(loadSound('sketch/assets/shooting_star/Dreams Illuminated_bip_104.wav'));
    shootingStarSoundFiles.addSoundFile(loadSound('sketch/assets/shooting_star/Dreams Illuminated_bip_203.wav'));
    shootingStarSoundFiles.addSoundFile(loadSound('sketch/assets/shooting_star/Dreams Illuminated_bip_302.wav'));
    shootingStarSoundFiles.addSoundFile(loadSound('sketch/assets/shooting_star/The Wanderer_bip01.wav'));
}
function setup() {
    createCanvas(windowWidth, windowHeight);
    neighbourFinder = new NeighbourFinder(stars, NEIGHBOUR_SEARCH_RADIUS);
    audioManager = new AudioManager();
    mousePointerGuide = createVector(0, 0);
    objectDetector = new ObjectDetector();
    objectDetector.init();
}
function mouseClicked() {
    if (!fullscreen()) {
        fullscreen(true);
        return;
    }
    if (!audioManager.started) {
        audioManager.startAudio();
        noCursor();
        createStars(NUM_STARTING_STARS);
    }
    stars.push(createStar(mouseX, mouseY));
}
function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}
function createStar(x, y) {
    var star = new Star();
    star.x = x;
    star.y = y;
    star.radius = random(1, 3);
    star.velX = 0;
    star.velY = 0;
    neighbourFinder.addStar(star);
    return star;
}
function createStars(n) {
    for (var i = 0; i < n; i++) {
        stars.push(createStar(random(0, width), random(0, height)));
    }
}
function getCloseStars(star) {
    return neighbourFinder.query(star);
}
function getRandomStar() {
    if (stars.length == 0)
        return undefined;
    var index = int(random(stars.length));
    return stars[index];
}
function updateConstellations() {
    if (random() > 0.01) {
        return;
    }
    var star = getRandomStar();
    if (star == undefined)
        return;
    var others = star.closeStars.filter(function (s) { return s.canAddConnection(); });
    if (others.length === 0) {
        return;
    }
    constellationSoundFiles.playRandomSound(map(star.x, 0, width, -1, 1));
    star.addConstellation(others[0]);
    for (var i = 1; i < others.length; i++) {
        var from = others[i - 1];
        var to = others[i];
        from.addConstellation(to);
    }
}
function createRandomStar() {
    if (random() > 0.1) {
        return;
    }
    stars.push(createStar(random(0, width), random(0, height)));
}
function createShootingStar() {
    if (random() > 0.001) {
        return;
    }
    var star = new Star();
    star.maxLifetime = 100;
    star.radius = random(1, 3);
    star.x = random(0, width);
    star.y = random(0, height);
    star.maxSpeed = 20;
    star.velX = random(-star.maxSpeed, star.maxSpeed);
    star.velY = random(-star.maxSpeed, star.maxSpeed);
    star.wrap = false;
    shootingStars.push(star);
    var soundFile = shootingStarSoundFiles.playRandomSound(map(star.x, 0, width, -1, 1));
    if (soundFile != null) {
        star.mysteriousSoundObject = soundFile;
    }
}
function drawObjectDetection() {
    objectDetector.getPoints().forEach(function (d) {
        fill(0, 255, 0);
        circle(d.x, d.y, 10);
    });
    objectDetector.draw();
}
function drawNeighbourSearchBins() {
    neighbourFinder.gridMap.forEach(function (v, k, m) {
        var x = parseInt(k.split(",")[0]) * NEIGHBOUR_SEARCH_RADIUS;
        var y = parseInt(k.split(",")[1]) * NEIGHBOUR_SEARCH_RADIUS;
        fill(255, 0, 0);
        text(v.size, x + 5, y + 10);
        noFill();
        stroke(255, 0, 0);
        rect(x, y, NEIGHBOUR_SEARCH_RADIUS, NEIGHBOUR_SEARCH_RADIUS);
    });
}
function placeStarsWherePeopleAre() {
    if (frameCount % 60 == 0) {
        objectDetector.detect(objectDetector);
        objectDetector.getPoints().forEach(function (p) {
            stars.push(createStar(p.x, p.y));
        });
    }
}
function updateStars() {
    stars.forEach(function (s) {
        s.move();
        neighbourFinder.updateStar(s);
        s.draw();
    });
    if (frameCount % 60 == 0) {
        stars.forEach(function (s) {
            s.closeStars = getCloseStars(s);
        });
    }
}
function updateShootingStars() {
    shootingStars.forEach(function (s) {
        s.move();
        if (s.mysteriousSoundObject !== undefined) {
            s.mysteriousSoundObject.pan(map(s.x, 0, width, -1, 1));
        }
        s.draw();
    });
}
function funeral() {
    stars = stars.filter(function (s) { return s.lifetimePercent < 1; });
    shootingStars = shootingStars.filter(function (s) { return s.x >= 0 && s.x <= width && s.y >= 0 && s.y <= height; });
}
function draw() {
    background(0);
    updateConstellations();
    createRandomStar();
    createShootingStar();
    placeStarsWherePeopleAre();
    updateStars();
    fill(255);
    text(frameRate(), 10, 20);
}
var SoundFileManager = (function () {
    function SoundFileManager() {
        this.soundFiles = [];
        this.verb = new p5.Reverb();
    }
    SoundFileManager.prototype.addSoundFile = function (soundFile) {
        this.soundFiles.push(soundFile);
    };
    SoundFileManager.prototype.playRandomSound = function (panning) {
        if (this.soundFiles.length == 0) {
            return null;
        }
        var index = int(random(0, this.soundFiles.length));
        var soundFile = this.soundFiles[index];
        if (soundFile.isPlaying()) {
            return;
        }
        soundFile.pan(constrain(panning, -1, 1));
        this.verb.process(soundFile, 15, 10);
        soundFile.play();
        return soundFile;
    };
    return SoundFileManager;
}());
var numStarsCreated = 0;
var Star = (function () {
    function Star() {
        this.maxSpeed = 0.01;
        this.wrap = true;
        this.id = numStarsCreated++;
        this.mysteriousSoundObject = undefined;
        this.maxConnections = random(0, 5);
        this.connections = [];
        this.maxLifetime = random(60 * 20, 60 * 70);
        this.lifetime = 0;
        this.closeStars = [];
    }
    Star.prototype.canAddConnection = function () {
        return this.connections.length < this.maxConnections;
    };
    Star.prototype.addConstellation = function (other) {
        if (!this.canAddConnection() || !other.canAddConnection()) {
            return;
        }
        var edge = new StarEdge();
        edge.from = this;
        edge.to = other;
        this.connections.push(edge);
        other.connections.push(edge);
    };
    Star.prototype.draw = function () {
        noStroke();
        var percent = (Math.exp(-40 * this.lifetimePercent) - 1) * (Math.exp(40 * (this.lifetimePercent - 1)) - 1);
        var colour = lerpColor(color(255, 0), color(255, 255), percent);
        var rShift = Math.abs(percent * Math.sin(this.lifetimePercent * 50));
        var r = constrain(red(colour) + rShift * 600, 0, 255);
        var g = constrain(green(colour), 0, 255);
        var b = constrain(blue(colour) - rShift * 100, 0, 255);
        colour = color(r, g, b, alpha(colour));
        fill(colour);
        circle(this.x, this.y, this.radius);
        this.connections.forEach(function (c) { return c.draw(); });
    };
    Star.prototype.move = function () {
        var _this = this;
        this.accX = 0;
        this.accY = 0;
        this.prevX = this.x;
        this.prevY = this.y;
        this.closeStars.forEach(function (o) {
            var deltaX = _this.x - o.x;
            var deltaY = _this.y - o.y;
            var dst = distSq(_this.x, _this.y, o.x, o.y);
            if (dst < 10 * 10) {
                return;
            }
            _this.accX -= lerp(0, deltaX * constrain(sqrt(dst) - 30, 0, 20), 0.001) * 0.01;
            _this.accY -= lerp(0, deltaY * constrain(sqrt(dst) - 30, 0, 20), 0.001) * 0.01;
        });
        this.accX += random(-0.001, 0.001);
        this.accY += random(-0.001, 0.001);
        this.velX += this.accX;
        this.velY += this.accY;
        if (this.velX > this.maxSpeed)
            this.velX = this.maxSpeed;
        if (this.velX < -this.maxSpeed)
            this.velX = -this.maxSpeed;
        if (this.velY > this.maxSpeed)
            this.velY = this.maxSpeed;
        if (this.velY < -this.maxSpeed)
            this.velY = -this.maxSpeed;
        this.x += this.velX;
        this.y += this.velY;
        if (this.wrap) {
            if (this.x > width) {
                this.x = 0;
            }
            if (this.x < 0) {
                this.x = width;
            }
            if (this.y > height) {
                this.y = 0;
            }
            if (this.y < 0) {
                this.y = height;
            }
        }
        this.connections.forEach(function (e) { return e.update(); });
        this.connections = this.connections.filter(function (e) { return e.distance < e.maxDistance && e.distancePercent < 1; });
        this.lifetime++;
        this.lifetimePercent = this.lifetime / this.maxLifetime;
    };
    return Star;
}());
//# sourceMappingURL=build.js.map