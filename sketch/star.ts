let numStarsCreated = 0;

class Star {
    // I used vectors before, but I found that they were slow when copying and creating
    // Instead, just use raw numbers
    x: number;
    y: number;

    prevX: number;
    prevY: number;

    velX: number;
    velY: number;

    accX: number;
    accY: number;

    radius: number;

    maxSpeed: number = 0.01;

    // Wrap around the screen
    wrap: boolean = true;

    // Always good to have an id (for equality)
    id: number = numStarsCreated ++;

    // Used for panning
    mysteriousSoundObject: p5.SoundFile | undefined = undefined;

    maxConnections: number = random(0, 5);
    connections: StarEdge[] = [];

    // Used in the neighbour finder
    mysteriousKey: string;

    maxLifetime: number = random(60 * 20, 60 * 70);
    lifetime: number = 0;
    lifetimePercent: number;

    closeStars: Star[] = [];

    /**
     * Can the star have a new connection, or is it full?
     * 
     * @returns Can add another connection
     */
    canAddConnection(): boolean {
        return this.connections.length < this.maxConnections;
    }

    /**
     * Add a connection (constellation) between another star.
     * 
     * @param other Other star
     */
    addConstellation(other: Star) {
        if (!this.canAddConnection() || !other.canAddConnection()) {
            return;
        }
        let edge = new StarEdge();
        edge.from = this;
        edge.to = other;
        this.connections.push(edge);
        other.connections.push(edge);
    }

    /**
     * Draws the star
     */
    draw() {
        noStroke();
        // Make the star fade in when it appears and fade out when it disappears.
        let percent = (Math.exp(-40 * this.lifetimePercent) - 1) * (Math.exp(40 * (this.lifetimePercent - 1)) - 1);
        let colour: p5.Color = lerpColor(color(255, 0), color(255, 255), percent);
        // Make the star shimmer
        let rShift = Math.abs(percent * Math.sin(this.lifetimePercent * 50));
        let r = constrain(red(colour) + rShift * 600, 0, 255);
        let g = constrain(green(colour), 0, 255);
        let b = constrain(blue(colour) - rShift * 100, 0, 255);
        colour = color(r, g, b, alpha(colour));
        fill(colour);
        circle(this.x, this.y, this.radius);

        // The star is responsible for drawing its constellations
        this.connections.forEach(c => c.draw());
    }

    move() {
        this.accX = 0;
        this.accY = 0;
        this.prevX = this.x;
        this.prevY = this.y;
        
        // Make the star tend towards its neighbours (calculate center of mass with neighbours)
        this.closeStars.forEach(o => {
            let deltaX = this.x - o.x;
            let deltaY = this.y - o.y;
            let dst = dist(this.x, this.y, o.x, o.y);
            if (dst < 10) {
                return;
            }
            this.accX -= lerp(0, deltaX * constrain(dst - 30, 0, 20), 0.001) * 0.01;
            this.accY -= lerp(0, deltaY * constrain(dst - 30, 0, 20), 0.001) * 0.01;
        });

        // Also introduce a bit of randomness in its movement
        this.accX += random(-0.001, 0.001);
        this.accY += random(-0.001, 0.001);

        this.velX += this.accX;
        this.velY += this.accY;

        // Cap the speed
        // I used to use constrain here but it was quite slow
        if (this.velX > this.maxSpeed) this.velX = this.maxSpeed;
        if (this.velX < -this.maxSpeed) this.velX = -this.maxSpeed;
        if (this.velY > this.maxSpeed) this.velY = this.maxSpeed;
        if (this.velY < -this.maxSpeed) this.velY = -this.maxSpeed;

        this.x += this.velX;
        this.y += this.velY;
            
        if (this.wrap) {
            if (this.x > width) {
                this.x = 0;
            }
            if (this.x < 0) {
                this.x = width;
            }
            if (this.y > height) {
                this.y = 0;
            }
            if (this.y < 0) {
                this.y = height;
            }
        }

        // Star is responsible for updating its edges
        this.connections.forEach(e => e.update());
        this.connections = this.connections.filter(e => e.distance < e.maxDistance && e.distancePercent < 1);

        // Update its lifetime
        this.lifetime ++;
        this.lifetimePercent = this.lifetime / this.maxLifetime;
    }
}