// Ignore the type of this line (allegedly improves performance)
p5.disableFriendlyErrors = true;

// Keep the stars in arrays
let stars: Star[] = [];
let shootingStars: Star[] = [];

// Sound managers
let audioManager: AudioManager;
let constellationSoundFiles: SoundFileManager;
let shootingStarSoundFiles: SoundFileManager;

// Neighbour finding managers
const NEIGHBOUR_SEARCH_RADIUS: number = 40;
let neighbourFinder: NeighbourFinder;

const NUM_STARTING_STARS: number = 4000;

// Object detection manager
let objectDetector: ObjectDetector;


function preload() {
  // Load the sounds!
  constellationSoundFiles = new SoundFileManager();
  constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Dark Bellphone_bip01.wav'));
  constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Distant Obscure Beacon_bip02.wav'));
  constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Dreams Illuminated_bip01.wav'));
  constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Glacier Reflections_bip_104.wav'));
  constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Glacier Reflections_bip_203.wav'));
  constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Hall Of the Stone Church_bip07.wav'));
  constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Living Room_bip05.wav'));
  constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Lush Multi Layer 1_bip03.wav'));
  constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Stargazer_bip08.wav'));
  constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Subsonic_bip_106.wav'));
  constellationSoundFiles.addSoundFile(loadSound('sketch/assets/constellations/Vector Untitled_bip02.wav'));
  shootingStarSoundFiles = new SoundFileManager();
  shootingStarSoundFiles.addSoundFile(loadSound('sketch/assets/shooting_star/Dreams Illuminated_bip_104.wav'));
  shootingStarSoundFiles.addSoundFile(loadSound('sketch/assets/shooting_star/Dreams Illuminated_bip_203.wav'));
  shootingStarSoundFiles.addSoundFile(loadSound('sketch/assets/shooting_star/Dreams Illuminated_bip_302.wav'));
  shootingStarSoundFiles.addSoundFile(loadSound('sketch/assets/shooting_star/The Wanderer_bip01.wav'));
}

function setup() {
  createCanvas(windowWidth, windowHeight);

  // Initialize the managers
  neighbourFinder = new NeighbourFinder(stars, NEIGHBOUR_SEARCH_RADIUS);
  audioManager = new AudioManager();

  objectDetector = new ObjectDetector();
  objectDetector.init();
}

function mouseClicked() {
  // Set fullscreen (if not already)
  if (!fullscreen()) {
    fullscreen(true);
    return;
  }
  // Only at the start, start the audio, and create the stars
  if (!audioManager.started) {
    audioManager.startAudio();
    noCursor();
    createStars(NUM_STARTING_STARS);
  }
}

function windowResized() {
  // Update the screen size when resized
  resizeCanvas(windowWidth, windowHeight);
}

/**
 * Create a star at a given location
 * @param x X position
 * @param y Y position
 * @returns Star
 */
function createStar(x: number, y: number): Star {
  let star = new Star();
  star.x = x;
  star.y = y;
  star.radius = random(1, 3);
  star.velX = 0;
  star.velY = 0;
  neighbourFinder.addStar(star);
  return star;
}

/**
 * Create n stars
 * @param n Amount of stars to create
 */
function createStars(n: number) {
  for (let i = 0; i < n; i ++) {
    stars.push(createStar(random(0, width), random(0, height)));
  }
}

/**
 * Gets nearby stars
 * @param star Star
 * @returns Nearby stars
 */
function getCloseStars(star: Star): Star[] {
  // Just query the neighbour finder manager
  return neighbourFinder.query(star);
}

/**
 * Gets a random star (if there are any)
 * @returns Random star
 */
function getRandomStar(): Star | undefined {
  if (stars.length == 0) return undefined;
  let index = int(random(stars.length));
  return stars[index];
}

/**
 * Create a constellation
 */
function updateConstellations() {
  // Only create every now and then
  if (random() > 0.01) {
    return;
  }
  // Pick a star and add a constellation to it
  let star = getRandomStar();
  if (star == undefined) return;

  let others = star.closeStars.filter(s => s.canAddConnection());
  if (others.length === 0) {
    return;
  }
  // Trigger a sound effect!
  constellationSoundFiles.playRandomSound(map(star.x, 0, width, -1, 1));
  star.addConstellation(others[0]);
  for (let i = 1; i < others.length; i ++) {
    let from = others[i - 1];
    let to = others[i];
    from.addConstellation(to);
  }
}

/**
 * Create a star in a random position
 */
function createRandomStar() {
  // Only create every now an then
  if (random() > 0.1) {
    return;
  }
  stars.push(createStar(random(0, width), random(0, height)));
}

/**
 * Create a shooting star (speedy star)
 */
function createShootingStar() {
  // Very rare
  if (random() > 0.001) {
    return;
  }
  let star = new Star();
  star.maxLifetime = 100;
  star.radius = random(1, 3);
  star.x = random(0, width);
  star.y = random(0, height);
  // Speedy
  star.maxSpeed = 20;
  star.velX = random(-star.maxSpeed, star.maxSpeed);
  star.velY = random(-star.maxSpeed, star.maxSpeed);
  // Doesn't wrap
  star.wrap = false;
  shootingStars.push(star);
  // Play sound
  let soundFile = shootingStarSoundFiles.playRandomSound(map(star.x, 0, width, -1, 1));
  if (soundFile != null) {
    star.mysteriousSoundObject = soundFile;
  }
  
}

/**
 * Draw the object detection system's output
 */
function drawObjectDetection() {
  objectDetector.getPoints().forEach(d => {
    fill(0, 255, 0);
    circle(d.x, d.y, 10);
  });
  objectDetector.draw();
}

/**
 * Draw the neighbour finder manager bins (DEBUG)
 */
function drawNeighbourSearchBins() {
  neighbourFinder.gridMap.forEach((v, k, m) => {
    let x = parseInt(k.split(",")[0]) * NEIGHBOUR_SEARCH_RADIUS;
    let y = parseInt(k.split(",")[1]) * NEIGHBOUR_SEARCH_RADIUS;
    fill(255, 0, 0);
    text(v.size, x + 5, y + 10);
    noFill();
    stroke(255, 0, 0);
    rect(x, y, NEIGHBOUR_SEARCH_RADIUS, NEIGHBOUR_SEARCH_RADIUS);
  });
}

/**
 * From the location data (object detection), create a star there!
 */
function placeStarsWherePeopleAre() {
  if (frameCount % 60 == 0) {
    objectDetector.detect(objectDetector);
    objectDetector.getPoints().forEach(p => {
      stars.push(createStar(p.x, p.y));
    });
  }
}

/**
 * Update all stars
 */
function updateStars() {
  stars.forEach(s => {
    s.move();
    neighbourFinder.updateStar(s);
    s.draw();
  });
  // Get close stars only every now and then
  if (frameCount % 60 == 0) {
    stars.forEach(s => {
      s.closeStars = getCloseStars(s);
    });
  }
}

/**
 * Update all shooting stars
 */
function updateShootingStars() {
  shootingStars.forEach(s => {
    s.move();
    if (s.mysteriousSoundObject !== undefined) {
      // Pan the sounds to the x position of the stars
      s.mysteriousSoundObject.pan(map(s.x, 0, width, -1, 1));
    }
    s.draw();
  });
}

/**
 * Handle the dead stars
 */
function funeral() {
  stars = stars.filter(s => s.lifetimePercent < 1);
  // Delete shooting stars that are out of bounds
  shootingStars = shootingStars.filter(s => s.x >= 0 && s.x <= width && s.y >= 0 && s.y <= height);
}

function draw() {
  background(0);

  updateConstellations();
  createRandomStar();
  createShootingStar();
  placeStarsWherePeopleAre();

  updateStars();

  // Uncomment for debug information
  // drawObjectDetection();
  // drawNeighbourSearchBins();

  // FPS
  fill(255);
  text(frameRate(), 10, 20);
}