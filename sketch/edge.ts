class StarEdge {
    // Edge is a from, to pair
    from: Star;
    to: Star;
    
    // Weight of edge (distance)
    distance: number = 0;
    maxDistance: number = random(40, 60);
    distancePercent: number = 0;

    // Also has a lifetime
    lifetime: number = 0;
    maxLifetime: number = random(60 * 5, 60 * 15);
    lifetimePercent: number = 0;

    /**
     * Update the edge
     */
    update() {
        // Update distance
        this.distance = dist(this.from.x, this.from.y, this.to.x, this.to.y);
        this.distancePercent = this.distance / this.maxDistance;
        // Update lifetime
        this.lifetime ++;
        this.lifetimePercent = this.lifetime / this.maxLifetime;
    }

    /**
     * Draws the edge
     */
    draw() {
        // If has died
        if (this.distancePercent > 1) {
            return;
        }
        // Fade in and out with lifetime and distance
        let percent = 6.7 * this.distancePercent * (this.distancePercent - 1) * (this.distancePercent - 1);
        let o = 6.7 * this.lifetimePercent * (this.distancePercent - 1) * (this.distancePercent - 1);
        strokeWeight(0.5);
        stroke(lerpColor(color(255, 0), color(255, 180), percent * o));
        line(this.from.x, this.from.y, this.to.x, this.to.y);
    }
}