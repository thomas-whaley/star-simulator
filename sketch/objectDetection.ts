/**
 * Is an adaptor of ml5 objects (ml5 doesn't have types)
 */
class DetectedObject {
    x: number
    y: number
    w: number
    h: number
    // "person", "chair", ...
    label: string
    // In interval [0, 1]
    confidence: number
}

/**
 * Gets the image from the video camera and detects objects
 */
class ObjectDetector {
    detector: any
    video: p5.Element
    detections: DetectedObject[] = [];
    enabled: boolean = true;
    loaded: boolean = false;

    /**
     * Initialize the object detector (will be slow!)
     */
    init() {
        this.detector = ml5.objectDetector('cocossd');
        this.video = createCapture(VIDEO);
        this.video.hide();
    }

    /**
     * Update the object detector
     * @param self The ObjectDetector instance
     */
    detect(self: ObjectDetector) {
        // I have found that through these callbacks, it doesn't know what "this" is so we pass that through
        self.detector.detect(self.video, (e: any, r: any) => self.gotDetections(self, e, r));
    }

    /**
     * Callback to update the detected object array. This is called internally by the ml5 library.
     * @param self ObjectDetector instance
     * @param error Error
     * @param results Results
     */
    gotDetections(self: ObjectDetector, error: string | undefined, results: object[] | undefined) {
        if (!this.enabled) { return; }
        if (error) {
            console.error(error);
            return;
        }
        self.detections = results.map(r => {
            // Convert to the adaptor pattern
            let o = new DetectedObject();
            // Ignore the type errors here, ml5 doesn't have types!
            o.x = r.x;
            o.y = r.y;
            o.w = r.width;
            o.h = r.height;
            o.label = r.label;
            o.confidence = r.confidence;
            return o;
        })
        // Only check for humans!
        .filter(r => r.label == "person");
    }

    /**
     * Draw the detected objects.
     */
    draw() {
        this.detections.forEach(o => {
            stroke(0, 255, 0);
            strokeWeight(4);
            noFill();
            let x = map(o.x, 0, this.video.width, 0, width);
            let y = map(o.y, 0, this.video.height, 0, height);
            let w = map(o.w, 0, this.video.width, 0, width);
            let h = map(o.h, 0, this.video.height, 0, height);
            rect(x, y, w, h);
            strokeWeight(1);
            stroke(255, 0, 0);
            line(x, y, x + w, y + h);
            line(x, y + h, x + w, y);
            noStroke();
            fill(255);
            textSize(24);
            text(o.label, x + 10, y + 24);
        });
    }

    /**
     * Gets the center of each detected person
     * @returns Array of people positions
     */
    getPoints(): p5.Vector[] {
        return this.detections.map(o => createVector(
            map(o.x + o.w / 2, 0, this.video.width, 0, width),
            map(o.y + o.h / 2, 0, this.video.height, 0, height)
        ));
    }
}