/**
 * Neighbour finder is an optimized way of finding the neighbours within a given radius.
 * Instead of searching every star and finding if it is within a given radius, split the
 * screen into discrete bins of size (radius, radius), and have the stars live in their
 * corresponding bin.
 * 
 * This means that to find the neighbours of a star, you only need to search the stars in
 * the nearby bins. This speeds up the program a lot, and allowed me to have 5,000 stars
 * (before object detection and sounds)
 */
class NeighbourFinder {
    points: Star[];
    radius: number;
    gridMap: Map<string, Set<Star>> = new Map();

    constructor(points: Star[], radius: number) {
        this.points = points;
        this.radius = radius;
        this.points.forEach(p => this.addStar(p));
    }

    /**
     * Add a star to the neighbour finder handler.
     * 
     * @param star Star to add
     */
    addStar(star: Star) {
        let key = this.makeKeyFromPosition(star.x, star.y);
        star.mysteriousKey = key;
        if (!this.gridMap.has(key)) {
            this.gridMap.set(key, new Set());
        }
        this.gridMap.get(key).add(star);
    }

    /**
     * Update star (potentially move bins)
     * 
     * @param star Star to update
     */
    updateStar(star: Star) {
        if (!this.starDifferentKey(star)) {
            return;
        }
        this.removeStar(star);
        this.addStar(star);
    }

    /**
     * Remove a star from the bins.
     * 
     * @param star Star to remove
     */
    removeStar(star: Star) {
        if (!this.gridMap.has(star.mysteriousKey)) {
            return;
        }
        // This could result in memory leaks depending on how javascript implements this
        let stars = this.gridMap.get(star.mysteriousKey);
        stars.delete(star);
        if (stars.size == 0) {
            this.gridMap.delete(star.mysteriousKey);
        }
    }

    /**
     * Has the star's bin changed?
     * 
     * @param star Star
     * @returns Bin change
     */
    starDifferentKey(star: Star): boolean {
        return this.makeKeyFromPosition(star.x, star.y) != this.makeKeyFromPosition(star.prevX, star.prevY);
    }

    /**
     * Generates a bin key from an x, y position (screen space)
     * @param x X coordinate
     * @param y Y coordinate
     * @returns Bin key
     */
    makeKeyFromPosition(x: number, y: number): string {
        let scaledX = int(x / this.radius);
        let scaledY = int(y / this.radius);
        return this.makeKey(scaledX, scaledY);
    }

    /**
     * Makes a key given a row and column (bin space)
     * @param x Column (bin space)
     * @param y Row (bin space)
     * @returns Bin key
     */
    makeKey(x: number, y: number): string {
        return int(x) + "," + int(y);
    }

    /**
     * Get the nearby stars from a star. Can specify the minimum radius it should search.
     * @param star Star to search from
     * @param minRadius Minimum radius to search
     * @returns Array of stars (neighbours)
     */
    query(star: Star, minRadius: number = 0): Star[] {
        let x = int(star.x / this.radius);
        let y = int(star.y / this.radius);
        let neighbours: Star[] = [];
        for (let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                let keyX = x + i;
                let keyY = y + j;
                let key = this.makeKey(keyX, keyY);
                if (!this.gridMap.has(key)) {
                    continue;
                }
                // Find all of the stars in these bins that are near enough
                this.gridMap.get(key).forEach(s => {
                    if (neighbours.length > star.maxConnections) {
                        return;
                    }
                    let dst = dist(s.x, s.y, star.x, star.y);
                    if (dst > this.radius * this.radius || dst < minRadius * minRadius) {
                        return;
                    }
                    neighbours.push(s);
                });
            }
        }
        return neighbours;
    }
}