class SoundFileManager {
    // Sound file manager is a collection of sound files
    soundFiles: p5.SoundFile[] = [];
    verb: p5.Reverb = new p5.Reverb();

    /**
     * Add a sound file to the manager.
     * 
     * @param soundFile Sound file
     */
    addSoundFile(soundFile: p5.SoundFile) {
        this.soundFiles.push(soundFile);
    }

    /**
     * Play a random sound
     * 
     * @param panning Panning [-1, 1]
     * @returns Played sound file or null
     */
    playRandomSound(panning: number): p5.SoundFile | null {
        if (this.soundFiles.length == 0) {
            return null;
        }
        let index = int(random(0, this.soundFiles.length));
        let soundFile = this.soundFiles[index];
        if (soundFile.isPlaying()) {
            return null;
        }
        soundFile.pan(constrain(panning, -1, 1));
        this.verb.process(soundFile, 15, 10);
        soundFile.play();
        return soundFile;
    }
}