class AudioManager {
    // Drone
    freq: number = 50
    res: number = 10
    osc: p5.Oscillator
    filter: p5.Filter
    verb: p5.Reverb
    started: boolean = false

    constructor() {
        this.osc = new p5.Oscillator(100, 'triangle');
        this.verb = new p5.Reverb();
        this.filter = new p5.LowPass();
        this.filter.freq(this.freq);
        this.filter.res(this.res);
        this.osc.disconnect();
        this.osc.connect(this.filter);
        this.osc.amp(0.3);
        this.osc.freq(this.freq);
    }

    /**
     * Start the drone
     */
    startAudio() {
        this.started = true;
        this.osc.start();
        this.verb.process(this.filter, 3, 2);
    }

    requestFreqChange(newFreq: number) {
        if (newFreq != this.freq) {
            this.freq = newFreq;
            this.filter.freq(newFreq, 5);
        }
    }

    requestResChange(newRes: number) {
        if (newRes != this.res) {
            this.res = newRes;
            this.osc.amp(newRes, 5);
        }
    }
}