# Star Simulator - CMPO385 Major Project

In this program, I simulate a galaxy and put ambient music to it

[Git link](https://gitlab.com/thomas-whaley/star-simulator.git)

## Instructions
I used VS Code to write this and used the inbuilt server to host it. Really any method of hosting it is fine.

It will ask for permission to use the camera, If you would not like the "person" tracking part to be active, then you can disable this.

Performance may vary depending on the machine, I am running an M1 Macbook Air and it runs at 40-50 fps on average

It is meant to be projected on a ceiling, but it can be enjoyed on a monitor!

Note that the repo doesn't include the sound files (storage space issue), so you can add your own!